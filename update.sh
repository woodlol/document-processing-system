echo -e "\e[30;48;5;82m Start \e[0;49m";
echo -e "\e[30;48;5;82m Git pull... \e[0;49m";
git pull
echo;

echo -e "\e[30;48;5;82m cache clear... \e[0;49m";
docker-compose exec php php bin/console cache:clear --env=prod --no-warmup
docker-compose exec php php bin/console cache:warmup --env=prod
echo;

echo -e "\e[30;48;5;82m End!! \e[0;49m";
