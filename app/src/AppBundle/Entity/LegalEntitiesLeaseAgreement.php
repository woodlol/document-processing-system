<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LegalEntitiesLeaseAgreementRepository")
 * @ORM\Table(name="legalEntitiesLeaseAgreement")
 */
class LegalEntitiesLeaseAgreement extends Contract
{
    public const TRANSLATION_FIELDS = [
        'Организация:',
        'Генеральный директор:',
        'Регистрационный знак:',
        'Идентификационный номер (VIN):',
        'Марка:',
        'Модель:',
        'Год выпуска ТС:',
        'Адрес:',
        'ИНН/КПП:',
        'Р/С:',
        'К/С:',
        'БИК:',
    ];

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $fileName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @ORM\Version
     */
    protected $dateCreated;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $title;
}
