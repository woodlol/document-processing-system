<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CargoContractRepository")
 * @ORM\Table(name="cargoContract")
 */
class CargoContract extends Contract
{
    public const TRANSLATION_FIELDS = [
        'Тип:',
        'Марка/модель:',
        'Регистрационный номер:',
        'Грузоподъемность:',
        'Тип груза:',
        'Характер груза',
        'ТН ВЭД:',
        'Ежесуточное количество перевозимого груза на единицу грузового транспортного средства (кг.):',
        'Годовой объем перевозимого груза на единицу грузового транспортного средства (кг.):',
    ];

    public const ADDRESS_FIELD_ONE   = 'addressone';
    public const ADDRESS_FIELD_TWO   = 'addresstwo';
    public const ADDRESS_FIELD_THREE = 'addressthree';

    public const TYPES_FOR_FIELDS = [
        Address::TYPE_LOADING_POINT   => self::ADDRESS_FIELD_ONE,
        Address::TYPE_CROSSING        => self::ADDRESS_FIELD_TWO,
        Address::TYPE_UNLOADING_POINT => self::ADDRESS_FIELD_THREE,
    ];

    public const TRANSLATE_FOR_ADRESS = [
        self::ADDRESS_FIELD_ONE   => 'Место погрузки',
        self::ADDRESS_FIELD_TWO   => 'Пересечение',
        self::ADDRESS_FIELD_THREE => 'Место разгрузки',
    ];

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $fileName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @ORM\Version
     */
    protected $dateCreated;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $title;
}
