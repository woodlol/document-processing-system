<?php

namespace AppBundle\Entity;

interface FileEntityInterface
{
    /**
     * @return null|string
     */
    public function getFileName(): ?string;

    /**
     * @param string $fileName
     *
     * @return static
     */
    public function setFileName(string $fileName): self;
}
