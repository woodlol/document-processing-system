<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PhysicalPersonsLeaseAgreementRepository")
 * @ORM\Table(name="physicalPersonsLeaseAgreement")
 */
class PhysicalPersonsLeaseAgreement extends Contract
{
    public const TRANSLATION_FIELDS = [
        'Ф.И.О:',
        'Регистрационный знак:',
        'Идентификационный номер (VIN):',
        'Марка:',
        'Модель:',
        'Год выпуска ТС:',
        'Адрес:',
        'ПАСПОРТ:',
        'ВЫДАН:',
        'КОД ПОДРАЗДЕЛЕНИЯ:',
    ];

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $fileName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @ORM\Version
     */
    protected $dateCreated;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $title;
}
