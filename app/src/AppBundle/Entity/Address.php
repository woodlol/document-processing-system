<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AddressRepository")
 * @ORM\Table(name="address")
 */
class Address
{
    public const TYPE_LOADING_POINT   = 1;
    public const TYPE_CROSSING        = 2;
    public const TYPE_UNLOADING_POINT = 3;

    public const ALL_TYPES = [
        self::TYPE_LOADING_POINT   => 'Место погрузки',
        self::TYPE_CROSSING        => 'Пересечение',
        self::TYPE_UNLOADING_POINT => 'Место разгрузки',
    ];

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     *
     * @return Address
     */
    public function setType(int $type): Address
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Address
     */
    public function setName(string $name): Address
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNamedType(): string
    {
        return self::ALL_TYPES[$this->type] ?? 'Address have not type';
    }

    /**
     * @return bool
     */
    public function isLoading(): bool
    {
        return $this->type === self::TYPE_LOADING_POINT;
    }

    /**
     * @return bool
     */
    public function isUnload(): bool
    {
        return $this->type === self::TYPE_UNLOADING_POINT;
    }

    /**
     * @return bool
     */
    public function isCrossing(): bool
    {
        return $this->type === self::TYPE_CROSSING;
    }

    /**
     * @return string
     */
    public function getCustomFieldForCargoContract(): string
    {
        return CargoContract::TYPES_FOR_FIELDS[$this->type];
    }
}
