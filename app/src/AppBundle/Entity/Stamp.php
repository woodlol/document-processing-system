<?php

namespace AppBundle\Entity;

use AppBundle\Component\FileSettings;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StampRepository")
 * @ORM\Table(name="stamps")
 */
class Stamp implements FileEntityInterface
{
    public const PAINTING_ID = 1;
    public const PAINTING_ID_PHY = 2;
    public const STAMP_ID    = 3;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $fileName;

    /**
     * @ORM\Column(type="datetime")
     * @ORM\Version
     * @var \DateTime
     */
    private $dateCreated;

    public function __construct()
    {
        $this->dateCreated = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Stamp
     */
    public function setTitle(string $title): Stamp
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     *
     * @return Stamp
     */
    public function setFileName(string $fileName): FileEntityInterface
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated(): \DateTime
    {
        return $this->dateCreated;
    }

    /**
     * @param \DateTime $dateCreated
     *
     * @return Stamp
     */
    public function setDateCreated(\DateTime $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * @return string
     */
    public function getImageSrc(): string
    {
        return DIRECTORY_SEPARATOR . FileSettings::STAMP_FOLDER . DIRECTORY_SEPARATOR . $this->fileName;
    }
}
