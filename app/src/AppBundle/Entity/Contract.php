<?php

namespace AppBundle\Entity;

class Contract implements FileEntityInterface
{
    public const CLASSES = [
        Document::CARGO_CONTRACT      => CargoContract::class,
        Document::PHYSICAL_PERSON_AGR => PhysicalPersonsLeaseAgreement::class,
        Document::LEGAL_PERSON_AGR    => LegalEntitiesLeaseAgreement::class,
    ];

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $fileName;

    /**
     * @var \DateTime
     */
    protected $dateCreated;

    /**
     * @var string
     */
    protected $title;

    public function __construct()
    {
        $this->dateCreated = new \DateTime();
    }

    /**
     * @return null|string
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     *
     * @return static
     */
    public function setFileName(string $fileName): FileEntityInterface
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated(): \DateTime
    {
        return $this->dateCreated;
    }

    /**
     * @param \DateTime $dateCreated
     */
    public function setDateCreated(\DateTime $dateCreated): void
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Contract
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }
}