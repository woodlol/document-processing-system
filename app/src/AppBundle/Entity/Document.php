<?php

namespace AppBundle\Entity;

use AppBundle\Component\FileSettings;
use AppBundle\Component\Helper\UpdateMergeFieldHelper;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DocumentRepository")
 * @ORM\Table(name="documents")
 */
class Document implements FileEntityInterface
{
    public const PHYSICAL_PERSON_AGR = 'PhysicalPersonsLeaseAgreement';
    public const LEGAL_PERSON_AGR    = 'LegalEntitiesLeaseAgreement';
    public const CARGO_CONTRACT      = 'CargoContract';

    public const RUSSIAN_TITLES = [
        self::PHYSICAL_PERSON_AGR => 'Договор аренды ФЗ',
        self::LEGAL_PERSON_AGR    => 'Договор аренды ЮР',
        self::CARGO_CONTRACT      => 'Договор Грузоперевозки',
    ];

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $fileName;

    /**
     * @var array
     *
     * @ORM\Column(type="array")
     */
    private $columns;

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Document
     */
    public function setTitle(string $title): Document
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     *
     * @return self
     */
    public function setFileName(string $fileName): FileEntityInterface
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * @return array
     */
    public function getColumns(): ?array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     *
     * @return Document
     */
    public function setColumns(array $columns): Document
    {
        $this->columns = $columns;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFilePath(): string
    {
        return FileSettings::DOC_FOLDER . DIRECTORY_SEPARATOR . $this->fileName;
    }

    /**
     * @return string
     */
    public function transformArrayToString(): string
    {
        return implode(', ', $this->columns ?? []);
    }

    /**
     * @throws \Exception
     *
     * @return array
     */
    public function getMergeFieldsWithoutNotUsedFields(): array
    {
        switch ($this->title) {
            case self::PHYSICAL_PERSON_AGR:
                return $this->getCombineArrayForTranslate(PhysicalPersonsLeaseAgreement::TRANSLATION_FIELDS);
            case self::LEGAL_PERSON_AGR:
                return $this->getCombineArrayForTranslate(LegalEntitiesLeaseAgreement::TRANSLATION_FIELDS);
            case self::CARGO_CONTRACT:
                return $this->getCombineArrayForTranslate(CargoContract::TRANSLATION_FIELDS);
            default:
                throw new \Exception('No translate');
        }
    }

    /**
     * @param array $translate
     *
     * @return array
     */
    private function getCombineArrayForTranslate(array $translate): array
    {
        $arr = array_combine($translate, array_unique(array_diff($this->columns, UpdateMergeFieldHelper::NOT_INPUT_VALUES[$this->title])));

        switch ($this->title) {
            case Document::PHYSICAL_PERSON_AGR:
                $arr = array_merge($arr, ['Роспись' => 'image' . STAMP::PAINTING_ID_PHY]);
            break;
            case Document::LEGAL_PERSON_AGR:
                $arr = array_merge($arr, ['Роспись' => 'image' . STAMP::PAINTING_ID, 'Штамп' => 'image' . STAMP::STAMP_ID]);
            break;

        }

        return $arr;
    }

    /**
     * @return string
     */
    public function getRussinTitle(): string
    {
        return self::RUSSIAN_TITLES[$this->title] ?? 'Not translate';
    }

    /**
     * @return bool
     */
    public function isCargo(): bool
    {
        return $this->title === self::CARGO_CONTRACT;
    }
}
