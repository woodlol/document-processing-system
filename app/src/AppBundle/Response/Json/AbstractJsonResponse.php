<?php

namespace AppBundle\Response\Json;

use Symfony\Component\HttpFoundation\JsonResponse;

abstract class AbstractJsonResponse extends JsonResponse
{
    public const STATUS_SUCCESS = 'success';

    public const STATUS_ERROR = 'error';

    protected static $stringStatus = self::STATUS_SUCCESS;

    /**
     * @param string $message
     * @param array  $data
     * @param int    $status
     * @param array  $headers
     */
    public function __construct($message = '', array $data = [], $status = 200, array $headers = [])
    {
        $data += [
            'status'  => static::$stringStatus,
            'message' => $message,
        ];

        parent::__construct($data, $status, $headers);
    }
}
