<?php

namespace AppBundle\Response\Json;

class ErrorJsonResponse extends AbstractJsonResponse
{
    protected static $stringStatus = self::STATUS_ERROR;

    /**
     * @param string|\Throwable $message
     * @param array             $data
     * @param int               $status
     * @param array             $headers
     */
    public function __construct($message = '', array $data = [], $status = 200, array $headers = [])
    {
        if ($message instanceof \Throwable) {
            $message = $message->getMessage();
        }

        parent::__construct($message, $data, $status, $headers);
    }
}
