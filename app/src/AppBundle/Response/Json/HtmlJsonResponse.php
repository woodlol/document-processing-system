<?php

namespace AppBundle\Response\Json;

use Symfony\Component\HttpFoundation\Response;

/**
 * Json response which should be used to return HTML markup of widgets and etc.
 */
class HtmlJsonResponse extends AbstractJsonResponse
{
    /**
     * @param Response|string $html
     * @param array           $data
     * @param string          $message
     * @param int             $status
     * @param array           $headers
     */
    public function __construct($html, array $data = [], $message = '', $status = 200, array $headers = [])
    {
        if ($html instanceof Response) {
            $html = $html->getContent();
        }

        $data['html'] = $html;

        parent::__construct($message, $data, $status, $headers);
    }
}
