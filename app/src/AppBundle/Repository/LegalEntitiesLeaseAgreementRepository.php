<?php

namespace AppBundle\Repository;

use AppBundle\Entity\LegalEntitiesLeaseAgreement;

/**
 * @method LegalEntitiesLeaseAgreement|null find($id, $lockMode = null, $lockVersion = null)
 * @method LegalEntitiesLeaseAgreement|null findOneBy(array $criteria, array $orderBy = null)
 * @method LegalEntitiesLeaseAgreement[]    findAll()
 * @method LegalEntitiesLeaseAgreement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LegalEntitiesLeaseAgreementRepository extends DefaultEntityRepository
{
}
