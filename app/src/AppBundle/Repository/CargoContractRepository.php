<?php

namespace AppBundle\Repository;

use AppBundle\Entity\CargoContract;

/**
 * @method CargoContract|null find($id, $lockMode = null, $lockVersion = null)
 * @method CargoContract|null findOneBy(array $criteria, array $orderBy = null)
 * @method CargoContract[]    findAll()
 * @method CargoContract[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CargoContractRepository extends DefaultEntityRepository
{
}
