<?php

namespace AppBundle\Repository;

use AppBundle\Component\Doctrine\Hydrators\ColumnHydrator;
use AppBundle\Component\Doctrine\Hydrators\CustomHydrators;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

abstract class DefaultEntityRepository extends EntityRepository implements ServiceEntityRepositoryInterface
{
    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager, $entityManager->getClassMetadata($this->getEntityClass()));

        $entityManager
            ->getConfiguration()
            ->addCustomHydrationMode((string) CustomHydrators::COLUMN_HYDRATOR, ColumnHydrator::class);
    }

    /**
     * Replace full repository class name to entity name.

     * If result does not match to entity it can be set manually in this method:
     *
     * @see StampRepository::getEntityClass()
     *
     * @return string
     */
    protected function getEntityClass(): string
    {
        return \mb_substr(\str_replace('Repository', 'Entity', static::class), 0, -6);
    }
}
