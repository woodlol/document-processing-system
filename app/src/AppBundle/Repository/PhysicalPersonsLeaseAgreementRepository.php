<?php

namespace AppBundle\Repository;

use AppBundle\Entity\PhysicalPersonsLeaseAgreement;

/**
 * @method PhysicalPersonsLeaseAgreement|null find($id, $lockMode = null, $lockVersion = null)
 * @method PhysicalPersonsLeaseAgreement|null findOneBy(array $criteria, array $orderBy = null)
 * @method PhysicalPersonsLeaseAgreement[]    findAll()
 * @method PhysicalPersonsLeaseAgreement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhysicalPersonsLeaseAgreementRepository extends DefaultEntityRepository
{
}
