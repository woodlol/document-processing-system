<?php

namespace AppBundle\Controller;

use AppBundle\Component\FileSaveHandler;
use AppBundle\Component\Services\StampFileService;
use AppBundle\Entity\Stamp;
use AppBundle\Form\Type\StampFormType;
use AppBundle\Repository\StampRepository;
use AppBundle\Response\Json\ErrorJsonResponse;
use AppBundle\Response\Json\HtmlJsonResponse;
use AppBundle\Response\Json\SuccessJsonResponse;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;

/**
 * @Route("/stamps", name="app_stamps_")
 */
class StampHandlerController extends AbstractController
{
    /**
     * @Route("/main", name="main")
     *
     * @param StampRepository $stampRepo
     *
     * @return Response
     */
    public function main(StampRepository $stampRepo): Response
    {
        return $this->render('@App/Stamp/main.html.twig', [
            'stamps' => $stampRepo->findAll(),
        ]);
    }

    /**
     * @Route("/new_stamp", name="new_stamp")
     *
     * @param Request          $request
     * @param StampFileService $fileService
     *
     * @return ErrorJsonResponse|HtmlJsonResponse|RedirectResponse
     */
    public function new(
        Request $request,
        StampFileService $fileService
    ): Response {
        try {
            $stamp = new Stamp();

            $form = $this->createForm(StampFormType::class, $stamp, [
                'action' => $this->generateUrl('app_stamps_new_stamp'),
            ]);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                /** @var Stamp $stamp */
                $stamp = $form->getData();

                $fileService->saveFile($form['fileName']->getData(), $stamp);
                $fileService->saveEntity($stamp);

                return $this->redirectToRoute('app_stamps_main');
            }

            $html = $this->renderView('@App/Stamp/new.html.twig', [
                'form' => $form->createView(),
            ]);

            return new HtmlJsonResponse($html);
        } catch (Throwable $exception) {
            return new ErrorJsonResponse($exception);
        }
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param Stamp                  $stamp
     * @param EntityManagerInterface $entityManager
     *
     * @return JsonResponse
     */
    public function remove(Stamp $stamp, EntityManagerInterface $entityManager): JsonResponse
    {
        try {
            $entityManager->remove($stamp);
            $entityManager->flush();

            return new SuccessJsonResponse();
        } catch (\Throwable $exception) {
            return new ErrorJsonResponse($exception);
        }
    }
}
