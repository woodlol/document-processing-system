<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Address;
use AppBundle\Entity\CargoContract;
use AppBundle\Repository\AddressRepository;
use AppBundle\Response\Json\ErrorJsonResponse;
use AppBundle\Response\Json\SuccessJsonResponse;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/address", name="app_address_")
 */
class AddressController extends AbstractController
{
    /**
     * @Route("/main", name="main")
     *
     * @param AddressRepository $addressRepository
     *
     * @return Response
     */
    public function main(AddressRepository $addressRepository): Response
    {
        return $this->render('@App/Address/main.html.twig', [
            'addresses' => $addressRepository->findAll(),
            'types' => CargoContract::TYPES_FOR_FIELDS,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete")
     *
     * @param Address                $address
     * @param EntityManagerInterface $entityManager
     *
     * @return JsonResponse
     */
    public function delete(Address $address, EntityManagerInterface $entityManager): JsonResponse
    {
        try {
            $entityManager->remove($address);
            $entityManager->flush();

            return new SuccessJsonResponse();
        } catch (\Throwable $exception) {
            return new ErrorJsonResponse($exception);
        }
    }

    /**
     * @Route("/create", name="create")
     *
     * @param Request                $request
     * @param EntityManagerInterface $entityManager
     *
     * @return JsonResponse
     */
    public function create(Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        try {
            $name = $request->get('name');
            $type = $request->get('type');

            if (null === $name) {
                return new ErrorJsonResponse('Введите имя');
            }

            if (null === $type) {
                return new ErrorJsonResponse('Введите тип');
            }

            if (!Address::ALL_TYPES[(int) $type]) {
                return new ErrorJsonResponse('Не верный тип');
            }

            $address = new Address();
            $address
                ->setName($name)
                ->setType((int) $type);

            $entityManager->persist($address);
            $entityManager->flush();

            return new SuccessJsonResponse($address->getNamedType() . ' ' . $address->getName() . ' создано');
        } catch (\Throwable $exception) {
            return new ErrorJsonResponse($exception);
        }

    }
}
