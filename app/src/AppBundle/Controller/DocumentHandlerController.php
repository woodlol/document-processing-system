<?php

namespace AppBundle\Controller;

use AppBundle\Component\Services\DocumentFileService;
use AppBundle\Entity\Document;
use AppBundle\Form\Type\DocumentFormType;
use AppBundle\Repository\DocumentRepository;
use AppBundle\Response\Json\ErrorJsonResponse;
use AppBundle\Response\Json\HtmlJsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @Route("/documents", name="app_documents_")
 */
class DocumentHandlerController extends AbstractController
{
    /**
     * @Route("/main", name="main")
     *
     * @param DocumentRepository $documentRepo
     *
     * @return Response
     */
    public function main(DocumentRepository $documentRepo): Response
    {
        return $this->render('@App/Documents/main.html.twig', [
            'documents' => $documentRepo->findAll(),
        ]);
    }

    /**
     * @Route("/create", name="create_new")
     *
     * @param Request             $request
     * @param DocumentFileService $fileService
     *
     * @return HtmlJsonResponse|RedirectResponse
     */
    public function new(Request $request, DocumentFileService $fileService): Response
    {
        try {
            $document = new Document();

            $form = $this->createForm(DocumentFormType::class, $document, [
                'action' => $this->generateUrl('app_documents_create_new'),
            ]);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                /** @var Document $document */
                $document = $form->getData();

                $fileService->saveFile($form['fileName']->getData(), $document);

                $mergeFields = $fileService->getArrayMergeFields($document->getFilePath());

                $fileService->saveEntity($document->setColumns($mergeFields));

                return $this->redirectToRoute('app_documents_main');
            }

            $html = $this->renderView('@App/Documents/new.html.twig', [
                'form' => $form->createView(),
            ]);

            return new HtmlJsonResponse($html);
        } catch (\Throwable $exception) {
            return new ErrorJsonResponse($exception);
        }
    }
}
