<?php

namespace AppBundle\Controller;

use AppBundle\Component\Services\ContractFileService;
use AppBundle\Entity\Contract;
use AppBundle\Entity\FileEntityInterface;
use AppBundle\Entity\PhysicalPersonsLeaseAgreement;
use AppBundle\Repository\ContractRepository;
use AppBundle\Repository\DocumentRepository;
use AppBundle\Repository\StampRepository;
use AppBundle\Response\Json\ErrorJsonResponse;
use AppBundle\Response\Json\HtmlJsonResponse;
use AppBundle\Response\Json\SuccessJsonResponse;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpWord\Exception\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/contracts", name="app_contracts_")
 */
class ContractController extends AbstractController
{
    /**
     * @Route("/main", name="main")
     *
     * @param ContractFileService $fileService
     * @param DocumentRepository  $documentRepo
     * @param StampRepository     $stampRepo
     *
     * @return Response
     */
    public function main(
        ContractFileService $fileService,
        DocumentRepository $documentRepo,
        StampRepository $stampRepo
    ): Response {
        return $this->render('@App/Contracts/main.html.twig', [
            'contracts' => $fileService->getAllContracts(),
            'documents' => $documentRepo->findAll(),
            'stamps'    => $stampRepo->findAll(),
            'addresses' => $fileService->getAddressForCargo(),
        ]);
    }

    /**
     * @Route("/create_contract", name="create_contract")
     *
     * @param Request             $request
     * @param ContractFileService $contractFileService
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function createContract(Request $request, ContractFileService $contractFileService): Response
    {
        $response = new RedirectResponse($this->generateUrl('app_contracts_main'));

        try {
            $response = $this->file($contractFileService->createAndGiveContractFromRequest($request));
        } catch (\Throwable $exception) {
            $this->addFlash('error', $exception->getMessage());
        }

        return $response;
    }

    /**
     * @Route("/download/{key}/{id}", name="download")
     *
     * @param string                 $key
     * @param int                    $id
     * @param EntityManagerInterface $entityManager
     *
     * @return BinaryFileResponse
     */
    public function download(string $key, int $id, EntityManagerInterface $entityManager): BinaryFileResponse
    {
        /** @var FileEntityInterface $entity */
        $entity = $entityManager->getRepository(Contract::CLASSES[$key])->find($id);

        return $this->file($entity->getFileName());
    }

    /**
     * @Route("/remove/{key}/{id}", name="remove")
     *
     * @param string                 $key
     * @param int                    $id
     * @param EntityManagerInterface $entityManager
     *
     * @return JsonResponse
     */
    public function remove(string $key, int $id, EntityManagerInterface $entityManager): JsonResponse
    {
        try {
            /** @var FileEntityInterface $entity */
            $entity = $entityManager->getRepository(Contract::CLASSES[$key])->find($id);

            $entityManager->remove($entity);
            $entityManager->flush();

            return new SuccessJsonResponse('Контракт был удалён.');
        } catch (\Throwable $exception) {
            return  new ErrorJsonResponse($exception);
        }
    }
}
