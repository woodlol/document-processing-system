<?php

namespace AppBundle\Component;

class DocumentParserComponent
{
    public const NAME_FOR_DOC_IN_ZIP = 'word/document.xml';

    public const START_OF_FIELD = '{{';

    public const END_OF_FIELD = '}}';

    /** @var \ZipArchive */
    private $zip;

    public function __construct()
    {
        $this->zip = new \ZipArchive();
    }

    /**
     * @param string $filePath
     *
     * @throws \Exception
     *
     * @return array
     */
    public function getMergeFieldsFromXml(string $filePath): array
    {
        return $this->getArrayFieldsFromXml($this->getXmlFromDocxZipFile($filePath));
    }

    /**
     * @param string $xml
     *
     * @throws \Exception
     *
     * @return array
     */
    private function getArrayFieldsFromXml(string $xml): array
    {
        preg_match_all('/' . self::START_OF_FIELD . '(.+)' . self::END_OF_FIELD . '/U', $xml, $match);

        if (!isset($match[1]) || 0 === count($match[1])) {
            throw new \Exception('File have not merge fields with ' . self::START_OF_FIELD . self::END_OF_FIELD);
        }

        return $match[1];
    }

    /**
     * @param string $filePath
     *
     * @return false|string
     */
    public function getXmlFromDocxZipFile(string $filePath)
    {
        $this->zip->open($filePath);

        return $this->zip->getFromName(self::NAME_FOR_DOC_IN_ZIP);
    }
}
