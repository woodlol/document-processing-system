<?php

namespace AppBundle\Component\Helper;

use AppBundle\Entity\CargoContract;
use AppBundle\Entity\Contract;
use AppBundle\Entity\Document;
use AppBundle\Entity\LegalEntitiesLeaseAgreement;
use AppBundle\Entity\PhysicalPersonsLeaseAgreement;
use Symfony\Component\HttpFoundation\Request;

class UpdateMergeFieldHelper
{
    private const VALUE_FOR_COUNT_FIO = 3;

    public const NOT_INPUT_VALUES = [
        Document::PHYSICAL_PERSON_AGR => [
            'date_for_id',
            'id',
            'date-created',
            'fio_short',
        ],
        Document::LEGAL_PERSON_AGR => [
            'date_for_id',
            'id',
            'date-created',
            'fio_short',
        ],
        Document::CARGO_CONTRACT => [
            'idnumcontract',
            'datecreatecontract',
            'addressone',
            'addresstwo',
            'addressthree',
        ],
    ];

    /** @var Request $request */
    private $request;

    /** @var array */
    private $mergeField;

    /** @var string */
    private $title;

    /** @var array */
    private $arrForPhysicalPersonsLeaseAgreement;

    /** @var array */
    private $arrForLegalEntitiesLeaseAgreement;

    /** @var array */
    private $arrForCargoContract;

    /**
     * @param Document $document
     * @param Request  $request
     */
    public function __construct(Document $document, Request $request)
    {
        $this->request    = $request;
        $this->mergeField = $document->getColumns();
        $this->title      = $document->getTitle();
    }

    /**
     * @throws \Exception
     *
     * @return array
     */
    public function getMergeFields(): array
    {
        $tmp = [];

        foreach ($this->mergeField as $filed) {
            $value = $this->request->get($filed);
            if (null === $this->request->get($filed)) {
                switch ($this->title) {
                    case Document::PHYSICAL_PERSON_AGR:
                        $tmp[$filed] = $this->arrForPhysicalPersonsLeaseAgreement[$filed];
                        break;
                    case Document::LEGAL_PERSON_AGR:
                        $tmp[$filed] = $this->arrForLegalEntitiesLeaseAgreement[$filed];
                        break;
                    case Document::CARGO_CONTRACT:
                        $tmp[$filed] = $this->arrForCargoContract[$filed];
                        break;
                    default:
                        throw new \Exception('');
                }
            } else {
                $tmp[$filed] = $value;
            }
        }

        return $tmp;
    }

    /**
     * @param int    $id
     * @param string $fio
     *
     * @throws \Exception
     * @return void
     */
    public function setArrForPhysicalPersonsAndLegalLeaseAgreement(int $id, string $fio): void
    {
        $now = new \DateTime();

        $m = explode(' ', $fio);

        if (self::VALUE_FOR_COUNT_FIO !== count($m)) {
            throw new \InvalidArgumentException('ФИО не коректное, нужно 3 слова.');
        }

        $arr = [
            'date_for_id'  => $now->format('d/m/y'),
            'id'           => $id,
            'date-created' => $this->getRusDate($now->format('Y/m/d')),
            'fio_short'    => $m[0] . ' ' . substr($m[1], 0, 2) . '. ' . substr($m[2], 0, 2) . '.',
        ];

        $this->arrForPhysicalPersonsLeaseAgreement = $arr;
        $this->arrForLegalEntitiesLeaseAgreement   = $arr;
    }

    /**
     * @param int    $id
     *
     * @throws \Exception
     * @return void
     */
    public function setArrForCargoContract(int $id): void
    {
        $now = new \DateTime();

        $this->arrForCargoContract = [
            'idnumcontract'      => $id,
            'datecreatecontract' => $this->getRusDate($now->format('Y/m/d')),
        ];
    }

    /**
     * @return Contract
     */
    public function getEntity(): Contract
    {
        switch ($this->title) {
            case Document::PHYSICAL_PERSON_AGR :
                return new PhysicalPersonsLeaseAgreement();
                break;
            case Document::LEGAL_PERSON_AGR :
                return new LegalEntitiesLeaseAgreement();
                break;
            case Document::CARGO_CONTRACT :
                return new CargoContract();
                break;
            default:
                throw new \InvalidArgumentException($this->title . ' not found.');
        }
    }

    /**
     * @param string $datetime
     *
     * @return string
     */
    private function getRusDate(string $datetime): string
    {
        $date = explode('/', $datetime);

        $month = [
            'января',
            'февраля',
            'марта',
            'апреля',
            'мая',
            'июня',
            'июля',
            'августа',
            'сентября',
            'октября',
            'ноября',
            'декабря',
        ];

        return ($date[2] > 0 ? '«' . $date[2] . "» " : '') . $month[$date[1] - 1] . " " . $date[0] . " г. ";
    }
}
