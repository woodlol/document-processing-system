<?php

namespace AppBundle\Component\Services;

use AppBundle\Component\FileSettings;

class StampFileService extends AbstractFileService
{
    protected $fileType = FileSettings::STAMP_TYPE;
}
