<?php

namespace AppBundle\Component\Services;

use AppBundle\Component\DocumentParserComponent;
use AppBundle\Component\FileSaveHandler;
use AppBundle\Component\FileSettings;
use Doctrine\ORM\EntityManagerInterface;

class DocumentFileService extends AbstractFileService
{
    protected $fileType = FileSettings::DOC_TYPE;

    /**
     * @var DocumentParserComponent
     */
    private $documentParserComponent;

    /**
     * @param EntityManagerInterface  $entityManager
     * @param FileSaveHandler         $fileSaveHandler
     * @param DocumentParserComponent $documentParserComponent
     */
    public function __construct(EntityManagerInterface $entityManager, FileSaveHandler $fileSaveHandler, DocumentParserComponent $documentParserComponent)
    {
        parent::__construct($entityManager, $fileSaveHandler);

        $this->documentParserComponent = $documentParserComponent;
    }

    /**
     * @param string $pathToFile
     *
     * @throws \Exception
     *
     * @return array
     */
    public function getArrayMergeFields(string $pathToFile): array
    {
        return $this->documentParserComponent->getMergeFieldsFromXml($pathToFile);
    }
}
