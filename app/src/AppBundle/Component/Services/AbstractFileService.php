<?php

namespace AppBundle\Component\Services;

use AppBundle\Component\FileSaveHandler;
use AppBundle\Entity\FileEntityInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

abstract class AbstractFileService
{
    /** @var EntityManagerInterface  */
    protected $entityManager;

    /** @var FileSaveHandler  */
    protected $fileSaveHandler;

    /** @var int */
    protected $fileType;

    /**
     * @param EntityManagerInterface $entityManager
     * @param FileSaveHandler        $fileSaveHandler
     */
    public function __construct(EntityManagerInterface $entityManager, FileSaveHandler $fileSaveHandler)
    {
        $this->entityManager   = $entityManager;
        $this->fileSaveHandler = $fileSaveHandler;
    }

    public function saveFile(UploadedFile $file, FileEntityInterface $entity): void
    {
        $this->fileSaveHandler->saveFile($file, $entity, $this->fileType);
    }

    /**
     * @param FileEntityInterface $entity
     */
    public function saveEntity(FileEntityInterface $entity): void
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

    /**
     * @param string $fileName
     *
     * @return string
     */
    public function getPathToFile(string $fileName): string
    {
        return $this->fileSaveHandler->getPathToFolderByType($this->fileType) . DIRECTORY_SEPARATOR . $fileName;
    }
}
