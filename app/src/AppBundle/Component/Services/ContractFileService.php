<?php

namespace AppBundle\Component\Services;

use AppBundle\Component\Contracts\ContractParserFactory;
use AppBundle\Component\DocumentParserComponent;
use AppBundle\Component\FileSaveHandler;
use AppBundle\Component\FileSettings;
use AppBundle\Component\Helper\UpdateMergeFieldHelper;
use AppBundle\Entity\CargoContract;
use AppBundle\Entity\Document;
use AppBundle\Entity\FileEntityInterface;
use AppBundle\Entity\Stamp;
use AppBundle\Repository\AddressRepository;
use AppBundle\Repository\CargoContractRepository;
use AppBundle\Repository\DocumentRepository;
use AppBundle\Repository\LegalEntitiesLeaseAgreementRepository;
use AppBundle\Repository\PhysicalPersonsLeaseAgreementRepository;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use ZipArchive;

class ContractFileService extends AbstractFileService
{
    protected $fileType = FileSettings::CONTRACT_TYPE;

    /** @var DocumentRepository */
    private $documentRepo;

    /** @var DocumentParserComponent */
    private $documentParserComponent;

    /** @var PhysicalPersonsLeaseAgreementRepository */
    private $physicalPersonsLeaseAgreementRepo;

    /** @var LegalEntitiesLeaseAgreementRepository */
    private $legalEntitiesLeaseAgreementRepo;

    /** @var CargoContractRepository */
    private $cargoContractRepo;

    /** @var AddressRepository  */
    private $addressRepo;

    /**
     * @param EntityManagerInterface                  $entityManager
     * @param FileSaveHandler                         $fileSaveHandler
     * @param DocumentRepository                      $documentRepo
     * @param DocumentParserComponent                 $documentParserComponent
     * @param PhysicalPersonsLeaseAgreementRepository $physicalPersonsLeaseAgreementRepo
     * @param LegalEntitiesLeaseAgreementRepository   $legalEntitiesLeaseAgreementRepo
     * @param CargoContractRepository                 $cargoContractRepo
     * @param AddressRepository                       $addressRepo
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        FileSaveHandler $fileSaveHandler,
        DocumentRepository $documentRepo,
        DocumentParserComponent $documentParserComponent,
        PhysicalPersonsLeaseAgreementRepository $physicalPersonsLeaseAgreementRepo,
        LegalEntitiesLeaseAgreementRepository $legalEntitiesLeaseAgreementRepo,
        CargoContractRepository $cargoContractRepo,
        AddressRepository $addressRepo
    ) {
        parent::__construct($entityManager, $fileSaveHandler);

        $this->documentRepo                      = $documentRepo;
        $this->documentParserComponent           = $documentParserComponent;
        $this->physicalPersonsLeaseAgreementRepo = $physicalPersonsLeaseAgreementRepo;
        $this->legalEntitiesLeaseAgreementRepo   = $legalEntitiesLeaseAgreementRepo;
        $this->cargoContractRepo                 = $cargoContractRepo;
        $this->addressRepo                       = $addressRepo;
    }

    /**
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return string
     */
    public function createAndGiveContractFromRequest(Request $request): string
    {
        $document = $this->documentRepo->findOneBy(['id' => (int) $request->get('documents')]);

        if (!$document instanceof Document) {
            throw  new InvalidArgumentException('Нет такого документа');
        }

        $mergeFieldHelper = new UpdateMergeFieldHelper($document, $request);

        $entity = $mergeFieldHelper->getEntity();
        $entity->setFileName('');

        $this->save($entity);

        $filePath = $this->getPathToDoc($document->getFileName(), FileSettings::CONTRACT_TYPE);
        $filePath = str_replace($document->getTitle() . '.docx', $document->getRussinTitle() . ' ' .$entity->getId() . '.docx', $filePath);

        switch ($document->getTitle()) {
            case Document::PHYSICAL_PERSON_AGR:
                $mergeFieldHelper->setArrForPhysicalPersonsAndLegalLeaseAgreement($entity->getId(),
                    $request->get('fio'));
                $entity->setTitle('Для ' . $request->get('fio'));
                break;
            case Document::LEGAL_PERSON_AGR:
                $mergeFieldHelper->setArrForPhysicalPersonsAndLegalLeaseAgreement($entity->getId(),
                    $request->get('fio_gen_dir'));
                $entity->setTitle('Для ' . $request->get('OOOrganizaciya') . ' ' . $request->get('fio_gen_dir'));
                break;
            case Document::CARGO_CONTRACT:
                $mergeFieldHelper->setArrForCargoContract($entity->getId());
                $entity->setTitle($request->get('field2') . ' ' . $request->get('field3'));
                break;
            default:
                throw new \Exception('Такого домуента для обработки нет.');
        }

        copy(
            $this->getPathToDoc($document->getFileName(), FileSettings::DOC_TYPE),
            $filePath
        );

        $zip = new ZipArchive();
        $zip->open($filePath);

        $this->updateStyleXml($zip);

        $images = [
            Stamp::PAINTING_ID     => trim($request->get('image' . Stamp::PAINTING_ID)),
            Stamp::STAMP_ID        => trim($request->get('image' . Stamp::STAMP_ID)),
            Stamp::PAINTING_ID_PHY => trim($request->get('image' . Stamp::PAINTING_ID_PHY)),
        ];

        $this->updateImageFile($images, $zip);

        $xml = $zip->getFromName(DocumentParserComponent::NAME_FOR_DOC_IN_ZIP);

        if ($document->isCargo()) {
            $newXml = $this->changeMergeFieldForCargo($xml, $mergeFieldHelper->getMergeFields());
        } else {
            $newXml = $this->changeMergeField($xml, $mergeFieldHelper->getMergeFields());
        }

        $zip->addFromString(DocumentParserComponent::NAME_FOR_DOC_IN_ZIP, $newXml);
        $zip->close();

        $this->save($entity->setFileName($filePath));

        return $filePath;
    }

    /**
     * @param string $xml
     * @param array  $mergeField
     *
     * @return string
     */
    private function changeMergeField(string $xml, array $mergeField): string
    {
        foreach ($mergeField as $field => $value) {
            $search = DocumentParserComponent::START_OF_FIELD . $field . DocumentParserComponent::END_OF_FIELD;

            $xml = str_replace($search, $value, $xml);
        }

        return $xml;
    }

    /**
     * @param string $fileName
     * @param int    $type
     *
     * @return string
     */
    private function getPathToDoc(string $fileName, int $type): string
    {
        return $this->fileSaveHandler->getPathToFolderByType($type) . DIRECTORY_SEPARATOR . $fileName;
    }

    /**
     * @param FileEntityInterface $entity
     */
    private function save(FileEntityInterface $entity): void
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

    /**
     * @return array
     */
    public function getAllContracts(): array
    {
        return [
            Document::PHYSICAL_PERSON_AGR => $this->physicalPersonsLeaseAgreementRepo->findAll(),
            Document::LEGAL_PERSON_AGR    => $this->legalEntitiesLeaseAgreementRepo->findAll(),
            Document::CARGO_CONTRACT      => $this->cargoContractRepo->findAll(),
        ];
    }

    /**
     * @param array      $array
     * @param ZipArchive $zipArchive
     */
    private function updateImageFile(array $array, ZipArchive $zipArchive): void
    {
        foreach ($array as $key => $value) {
            if (empty($value)) {
                continue;
            }

            $stamp = $this->entityManager->getRepository(Stamp::class)->findOneBy(['title' => $value]);

            if (!$stamp instanceof Stamp) {
                throw  new InvalidArgumentException('Нет такой печати ' . $value);
            }

            $zipArchive->addFromString(
                'word/media/image' . $key . '.png',
                file_get_contents($this->getPathToDoc($stamp->getFileName(), FileSettings::STAMP_TYPE))
            );
        }
    }

    /**
     * @param ZipArchive $zipArchive
     */
    private function updateStyleXml(ZipArchive $zipArchive): void
    {
        $xmlStyle = str_replace('zxx', 'ru', $zipArchive->getFromName('word/styles.xml'));

        $zipArchive->addFromString('word/styles.xml', $xmlStyle);
    }


    /**
     * @param string $xml
     * @param array  $mergeField
     *
     * @return string
     */
    private function changeMergeFieldForCargo(string $xml, array $mergeField): string
    {
        foreach ($mergeField as $field => $value) {
            $xml = str_replace($field, $value, $xml);
        }

        return $xml;
    }

    /**
     * @return array
     */
    public function getAddressForCargo(): array
    {
        $tmp = [
            CargoContract::ADDRESS_FIELD_ONE   => [],
            CargoContract::ADDRESS_FIELD_TWO   => [],
            CargoContract::ADDRESS_FIELD_THREE => [],
        ];

        foreach ($this->addressRepo->findAll() as $address) {
            $tmp[$address->getCustomFieldForCargoContract()][] = $address->getName();
        }

        return $tmp;
    }
}
