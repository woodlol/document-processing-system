<?php

namespace AppBundle\Component;

use AppBundle\Entity\FileEntityInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileSaveHandler
{
    /** @var Filesystem */
    private $fileSystem;

    /** @var string  */
    private $projectDir;

    /** @var EntityManagerInterface  */
    private $entityManager;

    /**
     * @param string                 $projectDir
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(string $projectDir, EntityManagerInterface $entityManager)
    {
        $this->projectDir    = $projectDir;
        $this->fileSystem    = new Filesystem();
        $this->entityManager = $entityManager;
    }

    /**
     * @param UploadedFile        $file
     * @param FileEntityInterface $entity
     * @param int                 $fileType
     */
    public function saveFile(UploadedFile $file, FileEntityInterface $entity, int $fileType): void
    {
        $fileName = $this->getNewFileName($file);

        $file->move($this->getPathToFolderByType($fileType), $fileName);

        $entity->setFileName($fileName);
    }

    /**
     * @param UploadedFile $file
     *
     * @return string
     */
    private function getNewFileName(UploadedFile $file): string
    {
        return implode('', [
            time(),
            '-',
            pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME),
            '.',
            $file->guessExtension(),
        ]);
    }

    /**
     * @param int $fileType
     *
     * @return string
     */
    public function getPathToFolderByType(int $fileType): string
    {
        return $this->projectDir . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . FileSettings::TYPES[$fileType];
    }
}
