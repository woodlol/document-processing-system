<?php

namespace AppBundle\Component;

class FileSettings
{
    public const STAMP_TYPE    = 1;
    public const DOC_TYPE      = 2;
    public const CONTRACT_TYPE = 3;

    private const FOLDER_FOR_FILES = 'files' . DIRECTORY_SEPARATOR;

    public const STAMP_FOLDER    = self:: FOLDER_FOR_FILES . 'Stamps';
    public const DOC_FOLDER      = self:: FOLDER_FOR_FILES . 'Docs';
    public const CONTRACT_FOLDER = self:: FOLDER_FOR_FILES . 'Contracts';

    public const TYPES = [
        self::STAMP_TYPE    => self::STAMP_FOLDER,
        self::DOC_TYPE      => self::DOC_FOLDER,
        self::CONTRACT_TYPE => self::CONTRACT_FOLDER,
    ];
}
