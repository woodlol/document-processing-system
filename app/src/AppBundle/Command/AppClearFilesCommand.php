<?php

namespace AppBundle\Command;

use AppBundle\Component\FileSaveHandler;
use AppBundle\Component\FileSettings;
use AppBundle\Component\Services\ContractFileService;
use AppBundle\Entity\FileEntityInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AppClearFilesCommand extends ContainerAwareCommand
{
    /** @var EntityManagerInterface  */
    private $entityManager;

    /** @var ContractFileService  */
    private $contractFileService;

    /** @var FileSaveHandler  */
    private $fileSaveHandler;

    /**
     * @param EntityManagerInterface $entityManager
     * @param ContractFileService    $contractFileService
     * @param FileSaveHandler        $fileSaveHandler
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ContractFileService $contractFileService,
        FileSaveHandler $fileSaveHandler
    ) {
        parent::__construct();

        $this->entityManager       = $entityManager;
        $this->contractFileService = $contractFileService;
        $this->fileSaveHandler     = $fileSaveHandler;
    }

    protected function configure()
    {
        $this
            ->setName('app:clear-files')
            ->setDescription('Clear files');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->entityManager->beginTransaction();

            foreach ($this->contractFileService->getAllContracts() as $contracts) {
                if (0 !== count($contracts)) {
                    /** @var FileEntityInterface $contract */
                    foreach ($contracts as $contract) {
                        $this->entityManager->remove($contract);
                    }
                }
            }

            $this->removeFilesFromFolder();

            $this->entityManager->flush();
            $this->entityManager->commit();

            $this->setIncrementFirst();

            $output->writeln('Success');
        } catch (\Throwable $exception) {
            if ($this->entityManager->getConnection()->isTransactionActive()) {
                $this->entityManager->rollback();
            }

            $output->writeln($exception->getMessage());
        };
    }

    private function removeFilesFromFolder(): void
    {
        $dirName = $this->fileSaveHandler->getPathToFolderByType(FileSettings::CONTRACT_TYPE);

        array_map('unlink', glob("$dirName/*.*"));
    }

    private function setIncrementFirst(): void
    {
        $this->entityManager->getConnection()->executeQuery('ALTER TABLE cargoContract AUTO_INCREMENT=0;');
        $this->entityManager->getConnection()->executeQuery('ALTER TABLE legalEntitiesLeaseAgreement AUTO_INCREMENT=0;');
        $this->entityManager->getConnection()->executeQuery('ALTER TABLE physicalPersonsLeaseAgreement AUTO_INCREMENT=0;');
    }
}
