<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190918145346 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE physicalPersonsLeaseAgreement ADD title VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE legalEntitiesLeaseAgreement ADD title VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cargoContract ADD title VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cargoContract DROP title');
        $this->addSql('ALTER TABLE legalEntitiesLeaseAgreement DROP title');
        $this->addSql('ALTER TABLE physicalPersonsLeaseAgreement DROP title');
    }
}
