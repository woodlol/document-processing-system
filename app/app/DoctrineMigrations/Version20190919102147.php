<?php

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20190919102147 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->connection->insert('documents', [
            'title'     => 'PhysicalPersonsLeaseAgreement',
            'file_name' => 'PhysicalPersonsLeaseAgreement.docx',
            'columns'   => 'a:15:{i:0;s:11:"date_for_id";i:1;s:2:"id";i:2;s:12:"date-created";i:3;s:3:"fio";i:4;s:8:"reg_znak";i:5;s:3:"vin";i:6;s:5:"marka";i:7;s:5:"model";i:8;s:4:"year";i:9;s:3:"fio";i:10;s:7:"address";i:11;s:7:"pasport";i:12;s:17:"pasport_kem_vydan";i:13;s:18:"kod_podrazdeleniya";i:14;s:9:"fio_short";}',
        ]);

        $this->connection->insert('documents', [
            'title'     => 'LegalEntitiesLeaseAgreement',
            'file_name' => 'LegalEntitiesLeaseAgreement.docx',
            'columns'   => 'a:17:{i:0;s:11:"date_for_id";i:1;s:2:"id";i:2;s:12:"date-created";i:3;s:14:"OOOrganizaciya";i:4;s:11:"fio_gen_dir";i:5;s:8:"reg_znak";i:6;s:3:"vin";i:7;s:5:"marka";i:8;s:5:"model";i:9;s:4:"year";i:10;s:14:"OOOrganizaciya";i:11;s:7:"address";i:12;s:7:"inn-kpp";i:13;s:3:"r-c";i:14;s:3:"k-c";i:15;s:3:"bik";i:16;s:9:"fio_short";}',
        ]);

        $this->connection->insert('documents', [
            'title'     => 'CargoContract',
            'file_name' => 'CargoContract.docx',
            'columns'   => 'a:14:{i:0;s:13:"idnumcontract";i:1;s:18:"datecreatecontract";i:2;s:8:"fieldone";i:3;s:8:"fieldtwo";i:4;s:10:"fieldthree";i:5;s:9:"fieldfour";i:6;s:10:"addressone";i:7;s:10:"addresstwo";i:8;s:12:"addressthree";i:9;s:9:"fieldfive";i:10;s:13:"fieldufivesix";i:11;s:8:"fieldsix";i:12;s:10:"fieldseven";i:13;s:10:"fieldeight";}',
        ]);
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('TRUNCATE TABLE documents');
    }
}